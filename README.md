# CD-Deploy Application from Jenkins Pipeline on EC2 Instance 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Tests](#test)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Installed Docker Compose on AWS EC2 Instance

* Created docker-compose.yml file that deploys our web application image

* Configured Jenkins pipeline to deploy newly built image using Docker Compose on EC2 server

* Improvement: Extracted multiple Linux commands that are executed on remote server into a separate shell script and executed the script from Jenkinsfile

* CI step: Increment version

* CI step: Built artifact for Java Maven application

* CI step: Built and push Docker image to Docker Hub

* CD step: Deployed new application version with Docker Compose

* CD step: Committed the version update

## Technologies Used

* AWS

* Jenkins

* Docker

* Linux 

* Git 

* Java 

* Maven 

* Docker Hub 

## Steps 

Step 1: download docker compose on EC2 instance

     DOCKER_CONFIG${DOCKER_CONFIG:-$HOME/.docker}
     mkdir -p $DOCKER_CONFIG/cli-plugins
     curl -SL https://github.com/docker/compose/releases/download/v2.12.2/docker-compose-linux-x86_64 -o $DOCKER_CONFIG/cli-plugins/docker-compose 

[Downloading docker compose](/images/01_downloading_docker_compose_manually.png)

Step 2: Give it execute permissions 

     chmod +x \$DOCKER_CONFIG/cli-plugins/docker-compose

[permissions](/images/02_giving_it_execute_permission.png)

Step 3: Add docker compose logic to Jenkinsfile 

[Jenkinsfile docker compose](/images/03_adding_docker_compose_logic_to_jenkinsfile.png)


step 4: Push changes to jenkinsfile to gitlab repo 

     git add .
     git commit -m "changes to jenkinsfile"
     git push 

Step 5: Check Pipeline success 

[Successful Pipeline](/images/04_successful_pipeline_job.png)
[Runninng Container on server](images/04_successful_pipeline_job.png)
[logs 1](/images/05_logs.png)

Step 6: Add docker compose to a shell scrpt

[docker compose in script](/images/07_adding_docker_compose_to_a_shell_script.png)


Step 7: Add script to Jenkinsfile 

[script in Jenkinsfile](/images/08_adding_script_to_jenkinsfile.png)

Step 8: Check Pipeline success 

[Pipeline Success](/images/09_successful_pipeline.png)
[logs 2](/images/10_logs.png)
[container on server](/images/11_containers_on_servers.png)
 

 Step 9: Enable dynamic tags 

 [Dynamic tags1](/images/12_enabling_dynamic_tags.png)
 [Dynamic tags2](/images/12_enabling_dynamic_tags2.png)
 [Dynamic tags3](/images/12_enabling_dynamic_tags3.png)


Step 10: push changes to gitlab and check if pipeline was successful

[Successful Pipeline 2](/images/13_successful_pipeline.png)
[server running containers](/imaes/14_running_containers.png)

Step 11: add versioning stage to Jenkinsfile

[versioning stage](/images/01_adding_versioning_stage.png)


Step 12: Insert needed environmental variables in build and deploy stages to enable dynamic versioning

[changes to build and deploy stages](/images/02_inserting_needed_environmental_variables_in_build_and_deploy_stage.png)

Stage 13: Insert a new stage for jenkins commit to enable accurate versioning 

[Jenkins commit](/images/03_inserting_jenkins_commit_to_enable_versioning.png)

Stage 14: push changes to gitlab 

stage 15: Check if pipeline was successful

[Success](/images/04_successful_logs.png)
[version bump](/images/05_version_bump.png)

## Installation

Run $ sudo apt install maven

## Usage 

Run $ java -jar java-maven-app*.jar 

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/complete-ci-cd-pipeline.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review

## Tests

Test were ran using mvn test.

## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/complete-ci-cd-pipeline

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.